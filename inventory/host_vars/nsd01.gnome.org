---
masters: &zone_default
  masters:
    - ip: 130.89.149.216

abisource_zones:
  - zone_name: abisource.com
    zone_filename: abisource.com
    <<: *zone_default
  - zone_name: abisource.org
    zone_filename: abisource.org
    <<: *zone_default
  - zone_name: abisuite.com
    zone_filename: abisuite.com
    <<: *zone_default
  - zone_name: abisuite.org
    zone_filename: abisuite.org
    <<: *zone_default
  - zone_name: abiword.com
    zone_filename: abiword.com
    <<: *zone_default
  - zone_name: abiword.org
    zone_filename: abiword.org
    <<: *zone_default

is_nsd_master: true
