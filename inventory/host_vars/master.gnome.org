ipaclient_mkhomedir: true
firewall_additional_rules:
  - "iptables -A INPUT -i eth1 -j ACCEPT"
firewall_allowed_tcp_ports:
  - 22
  - 873
